#include "mx_framebuffer.h"
#include "mx_rasterizers.h"
#include "mx_math.h"
#include "mx_mesh.h"
#include <stdio.h>

int main(void) {
    float counter = 0.f;
    const t_point_2 c = {15, 10};

    t_point_4 p0 = {5, 5, 1, 0};
    t_point_4 p1 = {35, 5, 1, 0};
    t_point_4 p2 = {35, 15, 1, 0};
    t_point_4 p3 = {5, 15, 1, 0};
    t_mat4 model;
    set_mat4_1(&model);
    t_mat4 view;
    set_mat4_trans(&view, (t_point_3 *){0, 0, -3});
    t_mat4 projection;
    const t_point_4 arr[6] = {p0, p1, p2, p0, p2, p3};

    t_mesh cube = {arr, 6, &model, &view, &projection};

    while (1) {
        mx_clear_framebuffer();
        
        // t_point_2 p0 = {5, 5};
        // t_point_2 p1 = {35, 5};
        // t_point_2 p2 = {35, 15};
        // t_point_2 p3 = {5, 15};

        // rotate_point(&c, &p0, counter);
        // rotate_point(&c, &p1, counter);
        // rotate_point(&c, &p2, counter);
        // rotate_point(&c, &p3, counter);

        counter += 0.2;

        // mx_rast_tri(&p0, &p1, &p2);
        // mx_rast_tri(&p2, &p3, &p0);

        // mx_do_antialising();
        mx_display_framebuffer();

        getchar();
    }
}
