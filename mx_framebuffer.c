#include "mx_framebuffer.h"
#include <unistd.h>

static char framebuffer[MX_FWIDTH * MX_FHEIGHT];
static int depthbuffer[MX_FWIDTH * MX_FHEIGHT];

char *mx_get_framebuffer() {
    return framebuffer;
}

void mx_clear_framebuffer() {
    for (int i = 0; i < MX_FHEIGHT; ++i) {
        for (int j = 0; j < MX_FWIDTH; ++j) {
            framebuffer[i * MX_FWIDTH + j] = ' ';
            depthbuffer[i * MX_FWIDTH + j] = 0;
        }
    }
}

void mx_set_pixel(int x, int y, char c) {
    if (x < 0 || y < 0 || x >= MX_FWIDTH || y >= MX_FHEIGHT) {
        return;
    }

    framebuffer[y * MX_FWIDTH + x] = c;
}

void mx_set_pixel_mesh(t_point_3 *p, char c) {
    p->x /= p->z;
    p->y /= p->z;

    if (p->x < 0 || p->y < 0 || p->x >= MX_FWIDTH || p->y >= MX_FHEIGHT) {
        return;
    }

    int pos = p->y * MX_FWIDTH + p->x;

    // TODO more or less???
    // if(p->z < depthbuffer[pos]) {
    //     return;
    // }

    // TODO: Post-processing

    framebuffer[pos] = c;
}

static char mx_get_pixel(int x, int y) {
    if (x < 0 || y < 0 || x >= MX_FWIDTH || y >= MX_FHEIGHT) {
        return ' ';
    }

    return framebuffer[y * MX_FWIDTH + x];
}

void mx_do_antialising() {
    for (int i = 0; i < MX_FWIDTH; ++i) {
        for (int j = 0; j < MX_FHEIGHT; ++j) {
            char up = mx_get_pixel(i, j - 1);
            char down = mx_get_pixel(i, j + 1);
            char left = mx_get_pixel(i - 1, j);
            char right = mx_get_pixel(i + 1, j);

            if(mx_get_pixel(i, j) == 'x') {
                // Верх
                if(up == 'x' && left == 'x' && right == ' ' && down == ' ') {
                    mx_set_pixel(i, j, 'F');
                    continue;
                }
                if(up == 'x' && left == ' ' && right == 'x' && down == ' ') {
                    mx_set_pixel(i, j, 'Y');
                    continue;
                }
                if(up == ' ' && left == 'x' && right == ' ' && down == 'x') {
                    mx_set_pixel(i, j, 'b');
                    continue;
                }
                if(up == ' ' && left == ' ' && right == 'x' && down == 'x') {
                    mx_set_pixel(i, j, 'd');
                    continue;
                }
                if(up == 'x' && left == ' ' && right == ' ' && down == ' ') {
                    mx_set_pixel(i, j, 'V');
                    continue;
                }
                if(up == ' ' && left == 'x' && right == ' ' && down == ' ') {
                    mx_set_pixel(i, j, '>');
                    continue;
                }
                if(up == ' ' && left == ' ' && right == 'x' && down == ' ') {
                    mx_set_pixel(i, j, '<');
                    continue;
                }
                if(up == ' ' && left == ' ' && right == ' ' && down == 'x') {
                    mx_set_pixel(i, j, ';');
                    continue;
                }
            }
        }
    }
}

void mx_display_framebuffer() {
    for (int i = 0; i < MX_FHEIGHT * MX_FWIDTH; i += MX_FWIDTH) {
        write(1, framebuffer + i, MX_FWIDTH);
        write(1, "\n", 1);
    }
}
