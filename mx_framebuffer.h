#pragma once
#include "mx_math.h"

#define MX_FWIDTH 100  // Framebuffer width
#define MX_FHEIGHT 40  // Framebuffer height

char *mx_get_framebuffer();
void mx_clear_framebuffer();
void mx_set_pixel(int x, int y, char c);
void mx_do_antialising();
void mx_display_framebuffer();

void mx_set_pixel_mesh(t_point_3 *p, char c);
