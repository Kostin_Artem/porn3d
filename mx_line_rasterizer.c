#include "mx_rasterizers.h"
#include "mx_framebuffer.h"

void mx_rast_line_y(const t_point_2 *p0, const t_point_2 *p1) {
    int min = mx_min(p0->y, p1->y);
    int max = mx_max(p0->y, p1->y);

    for (int y = min; y <= max; ++y) {
        mx_set_pixel(mx_x_coord(p0, p1, y), y, 'x');
    }
}

void mx_rast_line_x(const t_point_2 *p0, const t_point_2 *p1) {
    int min = mx_min(p0->x, p1->x);
    int max = mx_max(p0->x, p1->x);

    for (int x = min; x <= max; ++x) {
        mx_set_pixel(x, mx_y_coord(p0, p1, x), 'x');
    }
}

void mx_draw_hline(int x0, int x1, int y) {
    int min = mx_min(x0, x1);
    int max = mx_max(x0, x1);

    for (int x = min; x <= max; ++x) {
        mx_set_pixel(x, y, 'x');
    }
}

void mx_draw_vline(int y0, int y1, int x) {
    int min = mx_min(y0, y1);
    int max = mx_max(y0, y1);

    for (int y = min; y <= max; ++y) {
        mx_set_pixel(x, y, 'x');
    }
}
