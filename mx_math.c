#include "mx_math.h"
#include <math.h>

int mx_x_coord(const t_point_2 *p0, const t_point_2 *p1, float y) {
    return (float)(p1->x - p0->x) / (p1->y - p0->y) * (y - p0->y) + p0->x;
}

int mx_y_coord(const t_point_2 *p0, const t_point_2 *p1, float x) {
    return (float)(p1->y - p0->y) / (p1->x - p0->x) * (x - p0->x) + p0->y;
}

inline int mx_min(int a, int b) {
    return a < b ? a : b;
}

inline int mx_max(int a, int b) {
    return a > b ? a : b;
}

void mx_swap(int *a, int *b) {
    int temp = *a;
    *a = *b;
    *b = temp;
}

void rotate_point(const t_point_2 *center, t_point_2 *p, float angle) {
    float s = sin(angle);
    float c = cos(angle);

    p->x -= center->x;
    p->y -= center->y;

    float x0 = p->x * c - p->y * s;
    float y0 = p->x * s + p->y * c;

    p->x = x0 + center->x;
    p->y = y0 + center->y;
}

void set_mat4_0(t_mat4 *mat) {
    *mat = (t_mat4){ 0 };
}

void set_mat4_1(t_mat4 *mat) {
    *mat = (t_mat4){ 0 };
    mat->a00 = 1;
    mat->a11 = 1;
    mat->a22 = 1;
    mat->a33 = 1;
}

void mat4_transpose(t_mat4 *mat) {
    mx_swap(&mat->a01, &mat->a10);
    mx_swap(&mat->a02, &mat->a20);
    mx_swap(&mat->a03, &mat->a30);
    mx_swap(&mat->a12, &mat->a21);
    mx_swap(&mat->a13, &mat->a31);
    mx_swap(&mat->a23, &mat->a32);
}

void set_mat4_trans(t_mat4 *mat, t_point_3 *trans) {
    set_mat4_1(mat);
    mat->a03 = trans->x;
    mat->a13 = trans->y;
    mat->a23 = trans->z;
}

void set_mat4_rotatx(t_mat4 *mat, float angle) {
    float s = sin(angle);
    float c = cos(angle);

    *mat = (t_mat4){ 0 };

    mat->a00 = 1;
    mat->a33 = 1;
    mat->a11 = c;
    mat->a22 = c;
    mat->a21 = s;
    mat->a12 = -s;
}

void mult_matrix_4(t_mat4 *m1, t_mat4 *m2, t_mat4 *res) {
    res->a00 = m1->a00 * m2->a00 + m1->a01 * m2->a10 + m1->a02 * m2->a20 + m1->a03 * m2->a30;
    res->a01 = m1->a00 * m2->a01 + m1->a01 * m2->a11 + m1->a02 * m2->a21 + m1->a03 * m2->a31;
    res->a02 = m1->a00 * m2->a02 + m1->a01 * m2->a12 + m1->a02 * m2->a22 + m1->a03 * m2->a32;
    res->a03 = m1->a00 * m2->a03 + m1->a01 * m2->a13 + m1->a02 * m2->a23 + m1->a03 * m2->a33;

    res->a10 = m1->a10 * m2->a00 + m1->a11 * m2->a10 + m1->a12 * m2->a20 + m1->a13 * m2->a30;
    res->a11 = m1->a10 * m2->a01 + m1->a11 * m2->a11 + m1->a12 * m2->a21 + m1->a13 * m2->a31;
    res->a12 = m1->a10 * m2->a02 + m1->a11 * m2->a12 + m1->a12 * m2->a22 + m1->a13 * m2->a32;
    res->a13 = m1->a10 * m2->a03 + m1->a11 * m2->a13 + m1->a12 * m2->a23 + m1->a13 * m2->a33;

    res->a20 = m1->a20 * m2->a00 + m1->a21 * m2->a10 + m1->a22 * m2->a20 + m1->a23 * m2->a30;
    res->a21 = m1->a20 * m2->a01 + m1->a21 * m2->a11 + m1->a22 * m2->a21 + m1->a23 * m2->a31;
    res->a22 = m1->a20 * m2->a02 + m1->a21 * m2->a12 + m1->a22 * m2->a22 + m1->a23 * m2->a32;
    res->a23 = m1->a20 * m2->a03 + m1->a21 * m2->a13 + m1->a22 * m2->a23 + m1->a23 * m2->a33;

    res->a30 = m1->a30 * m2->a00 + m1->a31 * m2->a10 + m1->a32 * m2->a20 + m1->a33 * m2->a30;
    res->a31 = m1->a30 * m2->a01 + m1->a31 * m2->a11 + m1->a32 * m2->a21 + m1->a33 * m2->a31;
    res->a32 = m1->a30 * m2->a02 + m1->a31 * m2->a12 + m1->a32 * m2->a22 + m1->a33 * m2->a32;
    res->a33 = m1->a30 * m2->a03 + m1->a31 * m2->a13 + m1->a32 * m2->a23 + m1->a33 * m2->a33;
}

void mult_mat_point_4(t_mat4 *m, t_point_4 *p, t_point_4 *res) {
    res->x = m->a00 * p->x + m->a01 * p->y + m->a02 * p->z + m->a03 * p->w;
    res->y = m->a10 * p->x + m->a11 * p->y + m->a12 * p->z + m->a13 * p->w;
    res->z = m->a20 * p->x + m->a21 * p->y + m->a22 * p->z + m->a23 * p->w;
    res->w = m->a30 * p->x + m->a31 * p->y + m->a32 * p->z + m->a33 * p->w;
}

void set_mat4_proj(t_mat4 *mat, float near, float far, float aspect, float fov) {
    set_mat4_0(mat);
    mat->a32 = -1;
    // FLOAT. AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
}
