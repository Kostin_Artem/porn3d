#pragma once

typedef struct s_point_2 {
    int x;
    int y;
}              t_point_2;

typedef struct s_point_3 {
    int x;
    int y;
    int z;
}              t_point_3;

typedef struct s_point_4 {
    int x;
    int y;
    int z;
    int w;
}              t_point_4;

typedef struct s_mat4 {
    int a00;
    int a01;
    int a02;
    int a03;

    int a10;
    int a11;
    int a12;
    int a13;

    int a20;
    int a21;
    int a22;
    int a23;

    int a30;
    int a31;
    int a32;
    int a33;
}              t_mat4;

/*
 * Пропорции для алгоритма Брезенхэма
 */
int mx_x_coord(const t_point_2 *p0, const t_point_2 *p1, float y);
int mx_y_coord(const t_point_2 *p0, const t_point_2 *p1, float x);

int mx_min(int a, int b);
int mx_max(int a, int b);
void mx_swap(int *a, int *b);

void rotate_point(const t_point_2 *center, t_point_2 *p, float angle);

void set_mat4_0(t_mat4 *mat);
void set_mat4_1(t_mat4 *mat);
void mat4_transpose(t_mat4 *mat);
void set_mat4_trans(t_mat4 *mat, t_point_3 *trans);
void set_mat4_rotatx(t_mat4 *mat, float angle);
void set_mat4_proj(t_mat4 *mat, float near, float far, float aspect, float fov);
void mult_matrix_4(t_mat4 *m1, t_mat4 *m2, t_mat4 *res);
void mult_mat_point_4(t_mat4 *m, t_point_4 *p, t_point_4 *res);
