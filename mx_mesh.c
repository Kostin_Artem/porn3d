#include "mx_mesh.h"

t_point_3 mx_to_3d(t_mesh *mesh, t_point_4 *coord) {
    t_mat4 model_viev;
    t_mat4 trans;
    t_point_4 res_4;
    // TODO: Transpose??
    mult_matrix_4(mesh->model, mesh->view, &model_viev);
    mult_matrix_4(&model_viev, mesh->projection, &trans);
    mult_mat_point_4(&trans, coord, &res_4);
    return (t_point_3){res_4.x / res_4.w, res_4.y / res_4.w, res_4.z / res_4.w};
}

void mx_render_mesh(t_mesh *mesh) {
    t_point_3 p1;
    t_point_3 p2;
    t_point_3 p3;

    t_point_2 p01;
    t_point_2 p02;
    t_point_2 p03;

    for (int i = 0; i < mesh->v_count; i += 3) {
        p1 = mx_to_3d(mesh, mesh->vertices + i);
        p01.x = p1.x / p1.z;
        p01.y = p1.y / p1.z;

        p2 = mx_to_3d(mesh, mesh->vertices + i + 1);
        p02.x = p2.x / p2.z;
        p02.y = p2.y / p2.z;

        p3 = mx_to_3d(mesh, mesh->vertices + i + 2);
        p03.x = p3.x / p3.z;
        p03.y = p3.y / p3.z;

        mx_rast_tri(&p01, &p02, &p03);
    }
    // Преобразование координат для каждого треугольника
        // Умножение на все матрицы
        // Перевод в координаты буфера (80 х 25)
    // Отрисовка каждого треугольника
        // По-пиксельный depth-test
        // Отображение, если z > чем в буфере
        // Задавать функцию рендеринга пикселя для обработки освещения
}
