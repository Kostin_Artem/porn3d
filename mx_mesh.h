#pragma once
#include "mx_math.h"
#include "mx_framebuffer.h"
#include "mx_rasterizers.h"

typedef struct s_mesh {
    const t_point_4 *vertices;
    int v_count;
    t_mat4 *model;
    t_mat4 *view;
    t_mat4 *projection;
}              t_mesh;

t_point_3 mx_to_3d(t_mesh *mesh, t_point_4 *coord);

void mx_render_mesh(t_mesh *mesh);
