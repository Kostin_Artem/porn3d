#pragma once
#include "mx_math.h"

void mx_rast_line_y(const t_point_2 *p0, const t_point_2 *p1);
void mx_rast_line_x(const t_point_2 *p0, const t_point_2 *p1);
void mx_draw_hline(int x0, int x1, int y);
void mx_draw_vline(int y0, int y1, int x);

void mx_rast_tri(const t_point_2 *p0, const t_point_2 *p1, const t_point_2 *p2);
void mx_rast_tri_ready(const t_point_2 *p0, const t_point_2 *p1, const t_point_2 *p2);

