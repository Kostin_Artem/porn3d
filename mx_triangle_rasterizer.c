#include "mx_rasterizers.h"

/*
 * Mid: y0; Max: y1; Min: y2.
 */
void mx_rast_tri_ready(const t_point_2 *p0, const t_point_2 *p1, const t_point_2 *p2) {
    for (int y = p0->y; y <= p1->y; ++y) {
        if (p0->y == p1->y) {
            mx_draw_hline(p0->x, p1->x, y);
            break;
        } else {
            mx_draw_hline(mx_x_coord(p0, p1, y), mx_x_coord(p1, p2, y), y);
        }
    }
    for (int y = p0->y; y >= p2->y; --y) {
        if (p0->y == p2->y) {
            mx_draw_hline(p0->x, p2->x, y);
            break;
        } else {
            mx_draw_hline(mx_x_coord(p0, p2, y), mx_x_coord(p1, p2, y), y);
        }
    }
}

/*
 * Rasterize triangle on the buffer
 * TODO: pass char c to rasterize with it
 */
void mx_rast_tri(const t_point_2 *p0, const t_point_2 *p1, const t_point_2 *p2) {
    /*
     * Mid y0; Max: y1, y2.
     */
    if (p1->y >= p0->y && p0->y >= p2->y) {
        mx_rast_tri_ready(p0, p1, p2);
    } else if (p2->y >= p0->y && p0->y >= p1->y) {
        mx_rast_tri_ready(p0, p2, p1);
    /*
     * Mid y1; Max: y0, y2.
     */
    } else if(p0->y >= p1->y && p1->y >= p2->y) {
        mx_rast_tri_ready(p1, p0, p2);
    } else if(p2->y >= p1->y && p1->y >= p0->y) {
        mx_rast_tri_ready(p1, p2, p0);
    /*
     * Mid y2; Max: y0, y1.
     */
    } else if(p0->y >= p2->y && p2->y >= p1->y) {
        mx_rast_tri_ready(p2, p0, p1);
    } else if(p1->y >= p2->y && p2->y >= p0->y) {
        mx_rast_tri_ready(p2, p1, p0);
    }
}
